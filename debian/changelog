kdevelop-python (24.12.2-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 07 Feb 2025 14:42:47 +0000

kdevelop-python (24.12.1-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 09 Jan 2025 19:33:48 +0000

kdevelop-python (24.12.0-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 11 Dec 2024 18:30:01 +0000

kdevelop-python (24.08.3-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 07 Nov 2024 10:30:46 +0000

kdevelop-python (24.08.2-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 14 Oct 2024 21:03:33 +0000

kdevelop-python (24.08.1-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sun, 22 Sep 2024 07:40:14 +0000

kdevelop-python (24.08.0-0neon) noble; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 19 Jun 2024 09:57:53 +0000

kdevelop-python (24.05.0-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 20 Mar 2024 17:02:28 +0000

kdevelop-python (24.02.0-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Sat, 24 Feb 2024 03:47:51 +0000

kdevelop-python (23.08.5-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Mon, 19 Feb 2024 14:43:48 +0000

kdevelop-python (23.08.4-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 06 Dec 2023 16:26:15 +0000

kdevelop-python (23.08.3-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 10 Nov 2023 09:19:36 +0000

kdevelop-python (23.08.2-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 11 Oct 2023 17:03:31 +0000

kdevelop-python (23.08.1-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 14 Sep 2023 00:43:49 +0000

kdevelop-python (23.08.0-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 24 Aug 2023 09:30:53 +0000

kdevelop-python (23.04.2-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Tue, 06 Jun 2023 21:57:22 +0000

kdevelop-python (23.04.1-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 19 Apr 2023 15:35:26 +0000

kdevelop-python (22.12.3-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 02 Mar 2023 12:46:11 +0000

kdevelop-python (22.12.2-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 01 Feb 2023 17:13:00 +0000

kdevelop-python (22.12.0-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Wed, 07 Dec 2022 11:59:30 +0000

kdevelop-python (22.08.3-0neon) jammy; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Thu, 03 Nov 2022 09:21:55 +0000

kdevelop-python (22.08.3-0neon) focal; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 14 Oct 2022 17:55:12 +0000

kdevelop-python (22.08.3-0neon) focal; urgency=medium

  * New release

 -- Neon CI <neon@kde.org>  Fri, 09 Sep 2022 12:10:20 +0000

kdevelop-python (22.04.1-1) unstable; urgency=medium

  * New upstream release.
  * Update Vcs-* fields.
  * Bump Standards-Version to 4.6.1, no changes required.
  * CI: enable again the blhc job.
  * kdevelop-python is now part of the KDE release service, so:
    - switch watch file to release-service locations
    - import release-service GPG signing key
  * Update the build dependencies according to the upstream build system:
    - bump cmake to 3.16
    - bump Qt packages to 5.15.0
    - bump KF packages to 5.78.0
    - bump kdevelop-dev to 22.04.1
  * Bump KDEV_PLUGIN_VERSION to 36, according to upstream.
  * Modernize building:
    - add the dh-sequence-kf5 build dependency to use the kf5 addon
      automatically, removing pkg-kde-tools
    - drop the manually specified kf5 addon for dh
  * Remove the explicit as-needed linking, as it is done by binutils now.
  * Remove inactive Uploaders.
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Fri, 27 May 2022 11:03:57 +0200

kdevelop-python (5.6.2-2) unstable; urgency=medium

  * Use the debhelper 13 replacement features in install files:
    - switch from ${KDEV_PLUGIN_VERSION} to ${env:KDEV_PLUGIN_VERSION}
    - remove the dh-exec build dependency, no more needed
    - remove the lintian override for the old method
  * Bump Standards-Version to 4.6.0, no changes required.
  * CI: disable the blhc job.
  * Use the ${misc:Pre-Depends} substvar in kdevelop-python-data.

 -- Pino Toscano <pino@debian.org>  Sat, 25 Sep 2021 07:11:13 +0200

kdevelop-python (5.6.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.6.2, as indicated by the
    upstream build system.
  * Use execute_after_dh_auto_install to avoid invoking dh_auto_install
    manually.

 -- Pino Toscano <pino@debian.org>  Wed, 03 Feb 2021 10:24:33 +0100

kdevelop-python (5.6.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.6.1, as indicated by the
    upstream build system.
  * Remove the unused upstream GPG signing key.
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Wed, 09 Dec 2020 10:16:25 +0100

kdevelop-python (5.6.0-2) unstable; urgency=medium

  * Update the Python (build) dependencies according to the new default
    version in Debian:
    - switch the python3.8-dev build dependency to python3.9-dev
    - switch the python3.8 dependency to python3.9
  * Bump Standards-Version to 4.5.1, no changes required.
  * Switch the watch file to version 4, no changes required.
  * Add the upstream GPG signing key.

 -- Pino Toscano <pino@debian.org>  Fri, 20 Nov 2020 09:54:12 +0100

kdevelop-python (5.6.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.6.0, as indicated by the
    upstream build system.
  * Bump KDEV_PLUGIN_VERSION to 34, according to upstream.
  * Bump the debhelper compatibility to 13:
    - switch the debhelper-compat build dependency to 13
    - stop passing --fail-missing to dh_missing, as it is the default behaviour
  * Update lintian overrides.

 -- Pino Toscano <pino@debian.org>  Fri, 11 Sep 2020 16:51:17 +0200

kdevelop-python (5.5.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.5.2, as indicated by the
    upstream build system.
  * Set Rules-Requires-Root: no.
  * Fix typo in the 5.5.1-1 changelog stanza.

 -- Pino Toscano <pino@debian.org>  Thu, 04 Jun 2020 12:16:20 +0200

kdevelop-python (5.5.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.5.1, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Sun, 10 May 2020 07:35:31 +0200

kdevelop-python (5.5.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump the debhelper compatibility to 12:
    - switch the debhelper build dependency to debhelper-compat 12
    - remove debian/compat
  * Enable all the reprotest variations in the salsa CI.
  * Bump the kdevelop-dev build dependency to 5.5.0, as indicated by the
    upstream build system.
  * Use an environment variable for the version of plugins, to minimize changes
    to kdevelop-python.install
    - add the dh-exec build dependency
    - override the lintian info on that
  * Bump Standards-Version to 4.5.0, no changes required.
  * Update the Python (build) dependencies according to the new supported
    version:
    - switch the python3.7-dev build dependency to python3.8-dev
    - switch the python3.7 dependency to python3.8

 -- Pino Toscano <pino@debian.org>  Thu, 13 Feb 2020 21:38:38 +0100

kdevelop-python (5.4.6-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.4.6, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Thu, 09 Jan 2020 08:30:24 +0100

kdevelop-python (5.4.5-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.4.5, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Mon, 02 Dec 2019 22:43:35 +0100

kdevelop-python (5.4.4-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.4.4, as indicated by the
    upstream build system.
  * Bump Standards-Version to 4.4.1, no changes required.
  * Add the configuration for the CI on salsa.

 -- Pino Toscano <pino@debian.org>  Tue, 05 Nov 2019 08:30:44 +0100

kdevelop-python (5.4.2-2) unstable; urgency=medium

  * Team upload.
  * Depend on the newer ecm
  * Move qdebug files

 -- Maximiliano Curia <maxy@debian.org>  Sat, 21 Sep 2019 07:56:53 -0700

kdevelop-python (5.4.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.4.2, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Sat, 14 Sep 2019 20:17:51 +0200

kdevelop-python (5.4.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.4.1, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Fri, 16 Aug 2019 18:12:19 +0200

kdevelop-python (5.4.0-2) unstable; urgency=medium

  * Actually bump the kdevelop-dev build dependency to >= 4:5.4.0.

 -- Pino Toscano <pino@debian.org>  Sun, 11 Aug 2019 11:15:42 +0200

kdevelop-python (5.4.0-1) unstable; urgency=medium

  * New upstream release.
  * Update the build dependencies according to the upstream build system:
    - bump the Qt packages to >= 5.7.0
    - bump the KF packages to >= 5.28.0
    - bump kdevelop-dev to >= 5.4.0
    - explicitly add gettext
  * Update install files.

 -- Pino Toscano <pino@debian.org>  Sun, 11 Aug 2019 10:10:43 +0200

kdevelop-python (5.3.3-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.3.3, as indicated by the
    upstream build system.
  * Switch from --list-missing to --fail-missing for dh_missing; everything is
    installed already.

 -- Pino Toscano <pino@debian.org>  Sat, 20 Jul 2019 07:07:11 +0200

kdevelop-python (5.3.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.3.2, as indicated by the
    upstream build system.
  * Bump Standards-Version to 4.4.0, no changes required.
  * Drop the migration from kdevelop-python-dbg, no more needed after two
    Debian stable releases.

 -- Pino Toscano <pino@debian.org>  Tue, 09 Jul 2019 08:15:28 +0200

kdevelop-python (5.3.1-2) unstable; urgency=medium

  * Upload to unstable.
  * Replace the python-autopep8 (Python 2) recommend with python3-autopep8
    (Python 3).

 -- Pino Toscano <pino@debian.org>  Thu, 20 Dec 2018 22:40:02 +0100

kdevelop-python (5.3.1-1) experimental; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.3.1, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Tue, 18 Dec 2018 21:06:16 +0100

kdevelop-python (5.3.0-1) experimental; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.3.0, as indicated by the
    upstream build system.
  * Update install files.
  * Update the Python (build) dependencies according to the new supported
    version:
    - switch the python3.6-dev build dependency to python3.7-dev
    - switch the python3.6 dependency to python3.7
  * Bump Standards-Version to 4.2.1, no changes required.

 -- Pino Toscano <pino@debian.org>  Sun, 18 Nov 2018 07:58:47 +0100

kdevelop-python (5.2.4-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.2.4, as indicated by the
    upstream build system.
  * Bump Standards-Version to 4.2.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Tue, 21 Aug 2018 22:33:18 +0200

kdevelop-python (5.2.3-1) unstable; urgency=medium

  * New upstream release.
  * Switch Vcs-* fields to salsa.debian.org.
  * Bump the kdevelop-dev build dependency to 5.2.3, as indicated by the
    upstream build system.
  * Bump the debhelper compatibility to 11:
    - bump the debhelper build dependency to 11~
    - bump compat to 11
    - remove --parallel for dh, as now done by default
  * Switch from dh_install to dh_missing for --fail-missing.
  * Update install files.
  * Bump Standards-Version to 4.1.4, no changes required.

 -- Pino Toscano <pino@debian.org>  Tue, 22 May 2018 07:51:40 +0200

kdevelop-python (5.2.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevelop-dev build dependency to 5.2.1, as indicated by the
    upstream build system.

 -- Pino Toscano <pino@debian.org>  Tue, 28 Nov 2017 07:22:55 +0100

kdevelop-python (5.2.0-1) unstable; urgency=medium

  * New upstream release.
  * Drop the kdevplatform-dev build dependency, following the kdevplatform
    merge in kdevelop.
  * Change other build dependencies according to the upstream build system:
    - bump cmake to >= 3.0
    - bump qtbase-dev to >= 5.5.0
    - bump ECM and Frameworks to >= 5.15.0
  * Update install files.
  * Switch the watch file to https.
  * Switch Homepage to https.
  * Turn the versioned kdevelop dependency into a simple recommend, as it
    should be enough.
  * Bump Standards-Version to 4.1.1, no changes required.

 -- Pino Toscano <pino@debian.org>  Fri, 17 Nov 2017 14:29:29 +0100

kdevelop-python (5.1.2-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.1.0, no changes required.
  * Bump the kdevplatform-dev, kdevelop-dev, and kdevelop build & runtime
    dependencies to 5.1.2.

 -- Pino Toscano <pino@debian.org>  Tue, 29 Aug 2017 21:15:30 +0200

kdevelop-python (5.1.1-1) unstable; urgency=medium

  * New upstream release.
  * Bump the kdevplatform-dev, kdevelop-dev, and kdevelop build & runtime
    dependencies to 5.1.1.
  * Update the Python (build) dependencies according to the new supported
    version:
    - switch the python3.5-dev build dependency to python3.6-dev
    - switch the python3.5 dependency to python3.6
  * Update install files.
  * Bump Standards-Version to 4.0.0, no changes required.

 -- Pino Toscano <pino@debian.org>  Thu, 06 Jul 2017 12:08:19 +0200

kdevelop-python (5.0.3-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - Bump releated build dependencies.

 -- Lisandro Damián Nicanor Pérez Meyer <lisandro@debian.org>  Sat, 07 Jan 2017 17:46:06 -0300

kdevelop-python (5.0.1-2) unstable; urgency=medium

  * Upload to unstable.
  * Add missing epoch in the kdevelop-dev build dependency.

 -- Pino Toscano <pino@debian.org>  Tue, 20 Sep 2016 23:09:23 +0200

kdevelop-python (5.0.1-1) experimental; urgency=medium

  * New upstream release.
  * Bump the kdevplatform-dev, and kdevelop-dev build dependencies to >= 5.0.1.
  * Bump kdevelop dependency in kdevelop-python to >= 5.0.1.
  * Recommend pycodestyle, and python-autopep8, as they can be used for
    checking and fixing Python sources.

 -- Pino Toscano <pino@debian.org>  Tue, 20 Sep 2016 07:38:22 +0200

kdevelop-python (5.0-1) experimental; urgency=medium

  * New upstream release.
  * Update the build dependencies following the port to Frameworks:
    - add extra-cmake-modules, qtbase5-dev, libkf5i18n-dev,
      libkf5itemmodels-dev, libkf5kcmutils-dev, libkf5newstuff-dev,
      libkf5texteditor-dev, and libkf5threadweaver-dev
  * Bump build dependencies according to the upstream build system:
    - bump cmake to >= 2.8.12
    - bump kdevplatform-dev to >= 5.0
  * Update the Python (build) dependencies according to the new supported
    version: (Closes: #820186)
    - switch the python3.4-dev build dependency to python3.5-dev
    - switch the python3.4 dependency to python3.5
  * Add the kdevelop-dev build dependency.
  * Use the right dh addon:
    - switch from kde to kf5 dh addon
    - bump the pkg-kde-tools build dependency to >= 0.15.16
  * Disable building of unit tests, as long as they are not run.
  * Bump kdevelop dependency in kdevelop-python to >= 5.0.
  * Update install files.
  * Update lintian overrides.
  * Remove extra COPYING files; the only one currently is just a stub.
  * Split the growing set of data files into a new kdevelop-python-data.

 -- Pino Toscano <pino@debian.org>  Thu, 01 Sep 2016 23:14:05 +0200

kdevelop-python (1.7.3-1) unstable; urgency=medium

  [ Lisandro Damián Nicanor Pérez Meyer ]
  * New upstream release.
    - Bump kdevplatform-dev build dependency accordingly.
  * Improve debian/watch to search for different compression methods in
    tarballs and to repack the tarball with xz compression if necessary.
  * Remove existing patches, they are already applied upstream.

  [ Pino Toscano ]
  * Remove kdevelop-python-dbg in favour of the -dbgsym packages.
  * Bump Standards-Version to 3.9.8, no changes required.
  * Update Vcs-* fields.

 -- Pino Toscano <pino@debian.org>  Mon, 06 Jun 2016 23:09:15 +0200

kdevelop-python (1.7.2-2) unstable; urgency=medium

  * Backport upstream commit b54654be20cfd52cf121784f8a5dd6e77bc4767b to fix
    detection of the Python library on archs different than amd64 & i386;
    patch upstream_cmake-use-CMAKE_LIBRARY_ARCHITECTURE.patch.

 -- Pino Toscano <pino@debian.org>  Fri, 22 Jan 2016 08:27:27 +0100

kdevelop-python (1.7.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update watch file.
  * Bump kdevplatform-dev build dependency to >= 1.7.2.
  * Bump Standards-Version to 3.9.6, no changes required.
  * Explicitly use python3.4(-dev): kdev-python seems to require this
    particular version, so use it as stop-gap measure to make this plugin
    buildable and usable again. (Closes: #810750)
  * Backport upstream commit 673a4890fd3becfab22c1e9eda2071dafd08a161 to fix
    the macro of the Python executable; patch
    upstream_fix-cmake-macro-name-of-python-executable.patch.

 -- Pino Toscano <pino@debian.org>  Thu, 21 Jan 2016 23:45:57 +0100

kdevelop-python (1.7.0-1) unstable; urgency=medium

  [ Pino Toscano ]
  [ Andreas Cord-Landwehr ]
  * Initial release (Closes: #718296)

 -- Pino Toscano <pino@debian.org>  Tue, 23 Sep 2014 23:59:35 +0200
